﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SiCProject.DTOs;
using SiCProject.Models;

namespace SiCProject.Repositories
{
    public class ColecaoRepository : IColecaoRepository
    {

        private readonly SiCContext _context;
        private ProdutoRepository rep;

        public ColecaoRepository(SiCContext context)
        {
            _context = context;
            rep = new ProdutoRepository(context);
        }

        public ColecaoDTO getInfoColecaoDTO(Colecao c)
        {

            List<ProdutoDTO> meusProdutos = new List<ProdutoDTO>();


            foreach(ColecaoProduto cp in _context.ColecaoProduto)
            {
                if(cp.ColecaoID == c.ID)
                {
                    meusProdutos.Add(rep.getInfoProdutoDTO(_context.Produtos.Find(cp.ID)));
                }
            }
            Console.WriteLine("OMEGALUL - " + c.linhaEsteticaID);
            LinhaEstetica le = _context.LinhaEstética.Find(c.linhaEsteticaID);

            return new ColecaoDTO(c.ID, le.Nome, meusProdutos);
        }

        public List<Colecao> GetAll()
        {
            List<Colecao> cor = new List<Colecao>();
            foreach (Colecao corr in _context.Colecao)
            {
                cor.Add(this.GetByID(corr.ID));
            }
            return cor;
        }

        public Colecao GetByID(int id)
        {
            Colecao cor = _context.Colecao.Find(id);  

            return cor;
        }

        public Colecao Post(Colecao obj)
        {
            _context.Colecao.Add(obj);

            _context.SaveChangesAsync();

            return obj;
        }

        public bool Put(Colecao obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var col = _context.Colecao.Find(id);

            if (col == null)
            {
                return false;
            }

            _context.Colecao.Remove(col);
            _context.SaveChangesAsync();

            return true;
        }

    }
}
