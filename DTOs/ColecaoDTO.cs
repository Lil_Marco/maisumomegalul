﻿using SiCProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiCProject.DTOs
{
    public class ColecaoDTO
    {
        public ColecaoDTO(int iD, string linhaEstetica, List<ProdutoDTO> lProdutos)
        {
            this.ID = iD;
            this.LinhaEstetica = linhaEstetica;
            this.lProdutos = lProdutos;
        }

        public int ID { get; set; }
        public string LinhaEstetica { get; set; }

        public List<ProdutoDTO> lProdutos { get; set; }
    }
}
