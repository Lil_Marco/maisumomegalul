﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;

namespace SiCProject
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColecaoProdutoesController : ControllerBase
    {
        private readonly SiCContext _context;

        public ColecaoProdutoesController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/ColecaoProdutoes
        [HttpGet]
        public IEnumerable<ColecaoProduto> GetColecaoProduto()
        {
            return _context.ColecaoProduto;
        }

        // GET: api/ColecaoProdutoes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetColecaoProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var colecaoProduto = await _context.ColecaoProduto.FindAsync(id);

            if (colecaoProduto == null)
            {
                return NotFound();
            }

            return Ok(colecaoProduto);
        }

        // PUT: api/ColecaoProdutoes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutColecaoProduto([FromRoute] int id, [FromBody] ColecaoProduto colecaoProduto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != colecaoProduto.ID)
            {
                return BadRequest();
            }

            _context.Entry(colecaoProduto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ColecaoProdutoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ColecaoProdutoes
        [HttpPost]
        public async Task<IActionResult> PostColecaoProduto([FromBody] ColecaoProduto colecaoProduto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ColecaoProduto.Add(colecaoProduto);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetColecaoProduto", new { id = colecaoProduto.ID }, colecaoProduto);
        }

        // DELETE: api/ColecaoProdutoes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteColecaoProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var colecaoProduto = await _context.ColecaoProduto.FindAsync(id);
            if (colecaoProduto == null)
            {
                return NotFound();
            }

            _context.ColecaoProduto.Remove(colecaoProduto);
            await _context.SaveChangesAsync();

            return Ok(colecaoProduto);
        }

        private bool ColecaoProdutoExists(int id)
        {
            return _context.ColecaoProduto.Any(e => e.ID == id);
        }
    }
}